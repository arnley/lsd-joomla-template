$(function() {

  // General

  $('table').not('.kmsg').addClass('table'); // sexy tables
  $(':button, :submit').addClass('btn'); // sexy buttons
  $('[rel="tooltip"]').tooltip(); // sexy tooltips
  $('form .search').parent().remove(); // remove the search module
  $('#system-message').addClass('alert alert-block'); // sexy system messages
  $('.componentheading').each(function() { // sexy titles
    $(this).replaceWith($('<h2/>').html($(this).html()));
  });

  // Global layout

  if($('#rightblock').length == 0) {
    $('#centerblock').removeClass('span9');
  }

  // Shoutbox

  $('#KIDE_div').addClass('span3'); // set the width
  $('#KIDE_div').siblings('h4').hide(); // hide the title
  $('#KIDE_my_name, #KIDE_img_ajax').parent().hide(); // hide some elements
  $('#KIDE_txt').attr('placeholder', 'Saisir un message ici'); // set a placeholder
  // dynamic height
  update_shoutbox_height();
  $(window).resize(update_shoutbox_height);
  // remove the default styling
  remove_default_shoutbox_styling();
  window.setTimeout(remove_default_shoutbox_styling, 200);
  // toggling title
  //$('#KIDE_div h4').on('click', function() {
  //  $('#KIDE_div').toggleClass('inactive');
  //  $('#shoutbox_switcher').toggleClass('icon-resize-small');
  //});
  // dynamic placement
  $(window).scroll(function() {
    update_shoutbox_height();
    if(is_shoutbox_fixed()) {
      $('#rightblock').addClass('fixed');
    } else {
      $('#rightblock').removeClass('fixed');
    }
  });
  // input focus
  $('#KIDE_txt').focus(function() {
    $('#kideForm').addClass('active');
  }).blur(function() {
    $('#kideForm').removeClass('active');
  });
  // switchers actions
  $('#switch-tribune').click(function() {
    $('#KIDE_div').show();
  });
  $('#switch-modules').click(function() {
    $('#KIDE_div').hide();
  });
  // let youtube links work again
  //kide.bbcode.youtube = function(param) { return true }
  //$('#KIDE_output span a[href*="youtube.com/watch"]').attr('target', '_blank').attr('onclick', '');

  // Top menu

  $('#top-menu li.deeper').addClass('dropdown-submenu');
  $('#top-menu li.deeper > ul').addClass('dropdown-menu');

  // Latest posts

  $('.klatest-item a').tooltip(); // use tooltips for links

  // Forum

  // http://www.scorpions-du-desert.com/forum/4-le-site-web/43351-demande-distinction-des-messages-officiers
  $('a:contains("QG"), a:contains("[1-")', '#Kunena .ktopic-category').parents("tr td").siblings().addBack().css('background', '#f9d5bb');
  // sexy quotes
  $('#centerblock .kmsgtext-quote').each(function() {
    var message = $(this).parent();
    var quote_content = $(this);
    var author = $(this).prevAll('b').first();//.detach();
    console.log(author);
    // and move the author name below the quote
    var previous_tags = $(this).prevAll().slice(0,2);
    if( previous_tags.first().is('br') && previous_tags.last().is('b') ) {
      // remove the unecessary line returns
      previous_tags.first().remove();
//      var item = quote_content.next();
//      while(item.is('br')) {
//        item = item.next();
//        item.prev().remove();
//      }
      // formatted author name
      $('<small/>').text(author.text().replace(' écrit:', '')).appendTo(quote_content);
      author.remove();
    }
    // with <p> children
    $(this).wrapInner('<p/>');
    // <blockquote>
    $(this).replaceWith($('<blockquote class="kmsgtext-quote"/>').html($(this).html()));
  });

  // Top-right menu
  $('#Kunena .kprofilebox-link li').remove();
  $('#Kunena .kprofilebox-link').append(
    $('<li>').append(
      $('<a href="/forum/messagesrecents">Messages récents</a>')
    )
  );

  // Replace the "forum" link in the Kunena breadcrumbs
  $('#Kunena .path-element-first a').attr('href', '/forum');
  $('#Kunena .path-element-first a').text('Forum');

  // Fixes uploading attachments

  $('.kattachment .kfile-input.hidden').removeClass('kfile-input').removeClass('hidden');
  $('.kattachment .kfile-input-button.kbutton, .kattachment .kattachment-id-container, .kattachment .kfile-input-textbox').remove();

  $('.kattachment').show();
  $('.kattachment .kfile-input').removeClass('kfile-input');

  // Achievements

  var achievement_scores = [
    {
      name: 'kouyio',
      //achievements: 'ciel-winner petites-roulettes'
      achievements: 'petites-roulettes'
    },
    {
      name: 'lamare',
      achievements: 'love'
    },
    {
      name: '2161', // Wizard_912
      achievements: 'merde'
    },
    {
      name: 'Hayate',
      achievements: 'rockstar'
    },
    {
      name: 'ChartreuseFlambée',
      achievements: 'bbq'
    },
    {
      name: '62', // No_One
      achievements: 'realisateur'
    },
    {
      name: 'Koni',
      achievements: 'renommee'
    },
    {
      name: 'Diltyrr',
      achievements: 'suisse'
    },
    {
      name: 'Emerian',
      achievements: 'thune'
    },
    {
      name: 'Slave',
      achievements: 'scribe'
    },
    {
      name: 'MauJoQ',
      achievements: 'timing'
    },
    {
      name: 'ZFiRE',
      achievements: 'flood'
    },
    {
      name: 'Stivyx',
      achievements: 'realisateur'
    }
  ];
  for(var s = 0; s < achievement_scores.length; s++) {
    var score = achievement_scores[s];
    var name = score.name;
    var achievements = score.achievements.split(' ');
    for(var a = 0; a < achievements.length; a++) {
      var achievement = achievements[a];
      switch(achievement) {
        case 'ciel-winner':
          var winner = username_avatar(name);
          console.log('winner:', achievement);
          winner.addClass('ciel-winner');
          break;
        default:
          console.log('default', achievement);
          add_badge_to_user(name, achievement);
          break;
      }
    }
  }
});

// remove the stylesheet
function remove_default_shoutbox_styling() {
  $('link[href*="kide"]').remove();
}


function is_shoutbox_fixed() {
  return $(window).scrollTop() >= 160;
}

function update_shoutbox_height() {
  if(is_shoutbox_fixed()) {
    var height = window.innerHeight-122;
  } else {
    var height = $(window).height() + $(window).scrollTop() - 282;
  }
  $('#KIDE_msgs').height(height);
}

function username_avatar(username) {
  console.log('username_avatar', username);
  return $( '.kpost-avatar span.kavatar', $("#Kunena a[href $= 'userprofile/" + username + "']").parents(".kpost-profile") );
}

function badge_title(badge) {
  switch(badge) {
    case 'petites-roulettes': return "J'ai besoin de petites roulettes pour jouer";
    case 'classe': return "J'ai la méga classe";
    case 'alumettes': return "Je fume donc j'essuie";
    case 'doigts': return "Tu vois mon doigt ? Tu le vois hein ?!";
    case 'bbq': return "BBQ WTF OMG";
    case 'love': return "Je suis amour";
    case 'clef': return "J'ai une clé. Elle ouvre quoi ?";
    case 'timing': return "Maitrise les failles spatio-temporelles";
    case 'scribe': return "Je fais des bisous aux postulants";
    case 'attirant': return "Ressens-tu l'attirance que j'exerce sur ton être ?";
    case 'fax': return "J'ai un fax, tu aimes les 56k ?";
    case 'clin': return "Clin d'oeil langoureux";
    case 'realisateur': return "Silence ! on tourne...";
    case 'rockstar': return "Je suis une star du rock maghrébin";
    case 'renommee': return "Ma renommée est sans limite !";
    case 'thune': return "J'ai de l'oseille. OSEF des pauvres !";
    case 'photographe': return "Je photographie les otaries le mardi";
    case 'suisse': return "Je viens de Suisse, et vous ?";
    case 'flood': return "FLOOOOOOOOOOOOOD";
    case 'joueur-lol': return "Je joue à LoL. Viens te mesurer à moi dans la faille !";
    case 'joueur-ps2': return "Je joue à Planetside 2";
    case 'merde': return "Je suis une merde pleinement assumée";
  }
  return 'Pas de description pour ce badge';
}

function add_badge_to_user(username, badge) {
  console.log('add_badge_to_user', username, badge);
  var winner = username_avatar(username);
  var parent = winner.parents('.kpost-avatar');
  var achievements_div = $('.achievements', parent.parent()).first();
  if(achievements_div.size() == 0) {
    parent.before(
      $('<li class="achievements"></li>')
    );
  }
  var title = badge_title(badge);
  $('.achievements', parent.parent()).append(
    $('<span class="badge ' + badge + '" rel="tooltip" title="' + title +'" data-placement="right">')
  );
  $('.achievements .badge', parent.parent()).tooltip();

}
