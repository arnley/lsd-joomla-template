<?php
defined('_JEXEC') or die('Restricted access');

function modChrome_Lsd($module, &$params, &$attribs) {
  echo '<div class="module">';
  if ($module->showtitle) {
    echo '<h4>'.$module->title .'</h4>';
  }
  echo $module->content;
  echo '</div>';
}
?>
