<?php
/**
 * @copyright Copyright (C) 2012 Arnaud Leymet
 * http://arnley.com/
**/
defined('_JEXEC') or die;

$assets_path = JURI::base()."templates/".$this->template;

// User information
$user =& JFactory::getUser();
$user_id = $user->get('id');

// Template information
//$app =& JFactory::getApplication();
//$template = $app->getTemplate(true);
//$params = $template->params;
//$theme = $params->get('theme');
//$theme = $theme ? $theme : 'default'
//$theme = 'lsd';
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction ?>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction ?>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction ?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction ?>"> <!--<![endif]-->
<head>
  <jdoc:include type="head" />
  <meta charset="utf-8">
  <link rel="icon" type="image/png" href="<?php echo $assets_path ?>/img/themes/lsd/favicon16.png" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php echo $this->title ?></title>
  <meta name="google-site-verification" content="35tOySZgictQlYgxkP7fbfo8tAGAER8qIFXLKNUZM88" />
  <meta name="google-site-verification" content="XTkpupoLdNmsyFt7-tIMfwy2JntuK74TgEoiwkRWm2M" />
  <meta name="description" content="<?php echo $this->description ?>">
  <meta name="viewport" content="width=device-width">

  <link rel="stylesheet" href="<?php echo $assets_path ?>/css/bootstrap-united.min.css">
  <!-- <link rel="stylesheet" href="<?php echo $assets_path ?>/css/bootstrap-slate.min.css"> -->
  <link rel="stylesheet" href="<?php echo $assets_path ?>/css/bootstrap-responsive.min.css">
  <link rel="stylesheet" href="<?php echo $assets_path ?>/css/font-awesome.css">
  <!--[if lt IE 7]>
  <link rel="stylesheet" href="<?php echo $assets_path ?>/css/font-awesome-ie7.css">
  <![endif]-->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Londrina+Sketch">
  <link rel="stylesheet" href="<?php echo $assets_path ?>/css/main.css">

  <!--[if !IE 7]>
    <!--<style type="text/css">
      #wrap {display:table;height:100%}
    </style>-->
  <![endif]-->

  <script src="<?php echo $assets_path ?>/js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
</head>

<body data-spy="scroll" data-target=".navbar" id="home">
  <div id="wrap">
    <div id="main" class="lsd">
      <!--[if lt IE 9]>
        <p class="chromeframe">Vous utilisez un navigateur insipide. <a href="http://browsehappy.com/">Mettre à jour mon navigateur</a> ou <a href="http://www.google.com/chromeframe/?redirect=true">installer Google Chrome Frame</a> pour conserver mon navigateur mais avoir une meilleure expérience de navigation.</p>
      <![endif]-->

      <div class="pull-righty">
      <div class="header">

        <a href="/">
          <h1>
            <img src="<?php echo $assets_path ?>/img/themes/lsd/logo_LSD_25.png" alt="Les Scorpions du Désert" title="Les Scorpions du Désert">
            <!--<small>Guilde multi-jeux francophone</small>-->
          </h1>
        </a>

        <div class="dropdown pull-right" id="account">
          <?php if (!$user_id) { ?>
            <a href="/mon-profil/me-connecter"><i class="icon-signin"></i> Me connecter</a>
          <?php } else { ?>
            <?php
              // User avatar
              $db =& JFactory::getDBO();
              $sql = "SELECT avatar FROM #__comprofiler WHERE user_id= {$user_id}";
              $db->setQuery($sql);
              $field = $db->loadResult();
              $user_avatar = isset($field) ? "/images/comprofiler/" . $field : "default.png";
            ?>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#account">
              <img class="avatar" src="<?php echo $user_avatar; ?>">
              <b class="caret"></b>
              <span class="hidden-phone"><?php echo $user->username ?></span>
            </a>
            <ul class="dropdown-menu nav">
              <li><a href="/mon-profil/mon-profil"><i class="icon-pencil"></i> Mon profil</a></li>
              <li><a href="/messages-prives"><i class="icon-inbox"></i> Messages privés</a></li>
              <li><a href="/mon-profil/me-deconnecter"><i class="icon-signout"></i> Me déconnecter</a></li>
            </ul>
          <?php } ?>
        </div>

        <div class="birthday"><a href="/articles-battlefield-3/279-joyeux-anniversaire-les-10-ans-des-lsd">10 ans déjà !</a></div>
        <?php if (!$user_id) { ?>
          <!--
          <div id="recrutement">
            <span>
              <i class="icon-bullhorn icon-large"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Les LSD recrutent
            </span>
            <a href="/component/kunena/35-postuler--retour/18818-a-lire-avant-de-postuler" class="btn btn-primary btn-large"><i class="icon-arrow-right"></i> Nous rejoindre</a>
          </div>
          -->
        <?php } ?>
        <div class="navbar" data-spy="affix" data-offset-top="160">
          <div class="navbar-inner ribbon-wrapper">
            <div class="ribbon-edge-bottomleft"></div>
            <div id="top-menu">
              <ul class="nav pull-left">
                <li><a href="/">Accueil</a></li>
                <li><a href="/forum"><i class="icon-comments"></i> Forum</a></li>
                <?php if ($user_id) { ?>
                  <li><a href="/calendrier/month.calendar/<?php echo date('Y') ?>"><i class="icon-calendar"></i> Calendrier</a></li>
                <?php } ?>
                <li class="dropdown">
                  <a href="/2012-09-17-08-59-31" class="dropdown-toggle" data-toggle="dropdown">La guilde <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="/articles/liencharte"><i class="icon-check"></i> La charte des LSD</a></li>
                    <?php if ($user_id) { ?>
                      <li><a href="/articles/mumble"><i class="icon-headphones"></i> Tutoriel Mumble</a></li>
                    <?php } ?>
                    <li><a href="/articles/organisation"><i class="icon-sitemap"></i> Organisation</a></li>
                    <li><a href="/articles/histoirelsd"><i class="icon-info-sign"></i> Histoire des LSD</a></li>
                    <?php if ($user_id) { ?>
                      <li><a href="/articles/liste-des-joueurs"><i class="icon-group"></i> Liste des joueurs</a></li>
                    <?php } ?>
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sections <b class="caret"></b></a>
                  <jdoc:include type="modules" name="sections" />
                </li>
              </ul>
              <?php if ($user_id) { ?>
                <ul class="nav nav-pills pull-right hidden-phone" style="width: 24.5%">
                  <li class="active"><a id="switch-tribune" href="#" data-toggle="tab">Tribune</a></li>
                  <li><a id="switch-modules" href="#" data-toggle="tab">Modules</a></li>
                </ul>
              <?php } ?>
              <form class="navbar-search form-search pull-right visible-desktop" action="/component/search" method="get">
                <input type="hidden" name="searchphrase" value="all"></input>
                <div class="input-prepend">
                  <button type="submit" class="btn btn-primary">Rechercher</button><input type="search" name="searchword" class="search-query" placeholder="dans les billets, articles"></input>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="section">
        <div class="container-fluid">

          <div class="row-fluid">

            <?php if ($this->countModules('left')) { ?>
              <div id="leftblock" class="span3 hidden-phone">
                <jdoc:include type="modules" name="left" style="lsd" />
              </div>
            <?php } ?>

            <div id="centerblock" class="span9">
              <jdoc:include type="message" />
              <jdoc:include type="component" />
            </div>

            <?php if ($this->countModules('right')) { ?>
              <div id="rightblock" class="span3 hidden-phone">
                <jdoc:include type="modules" name="right" style="lsd" />
              </div>
            <?php } ?>

          </div>

        </div>
      </div>
      </div>
    </div>
  </div>

  <div id="footer">
    <div class="container">
      <div class="row">
        <div class="span4">
          <h4>Rejoignez-nous</h4>
          <ul class="unstyled">
            <li><a href="/forum"><i class="icon-comments"></i> sur nos forums</a></li>
          <li><a href="http://steamcommunity.com/groups/scorpions-du-desert?l=french" target="_blank"><img src="<?php echo $assets_path ?>/img/steam.png" style="margin-right: 2px" /> sur Steam</a></li>
            <li><a href="https://www.facebook.com/LSDGuilde" target="_blank"><i class="icon-facebook"></i> sur Facebook</a></li>
            <li><a href="https://twitter.com/LSDGuilde" target="_blank"><i class="icon-twitter"></i> sur Twitter</a></li>
            <li><a href="https://plus.google.com/b/108866269308609829845/108866269308609829845/posts" target="_blank"><i class="icon-google-plus"></i> sur Google+</a></li>
            <li><a href="http://pinterest.com/lsdguilde/" target="_blank"><i class="icon-pinterest"></i> sur Pinterest</a></li>
            <li><a href="http://www.youtube.com/user/LesScorpionsduDesert/" target="_blank"><img src="<?php echo $assets_path ?>/img/youtube.png" style="margin-right: 2px" /> sur Youtube</a></li>
            <?php if (!$user_id) { ?>
              <!--
              <br>
              <li><a href="/component/kunena/35-postuler--retour/18818-a-lire-avant-de-postuler" class="btn btn-primary"><i class="icon-ok"></i> Je postule</a></li>
              -->
            <?php } ?>
          </ul>
        </div>
        <div class="span8">
          <h4>Billets récents</h4>
          <div style="margin-right: 30%">
            <jdoc:include type="modules" name="billets" />
          </div>
        </div>

      </div>
    </div>
  </div>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo $assets_path ?>/js/vendor/jquery-1.8.1.min.js"><\/script>')</script>

  <script src="<?php echo $assets_path ?>/js/vendor/bootstrap.min.js"></script>

  <script src="<?php echo $assets_path ?>/js/main.js"></script>

  <script>
    var _gaq=[['_setAccount','UA-1810262-2'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>

</body>
</html>
